import { GifsServiceService } from './../../gifs/services/gifs-service.service';
import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
//Vamos a realizar un bucle para colocar los resultados del historial
//Los GET se usan como VARIABLES normales en el HTML
get historial(){
  return this.gifsService.historial
}

  constructor(private gifsService:GifsServiceService) { }

  ngOnInit(): void {
  }

  findGif=(hist:string)=>{
    this.gifsService.findGifs(hist)
  }
}

