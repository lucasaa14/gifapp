import { GifsServiceService } from './../services/gifs-service.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.scss']
})
export class FindComponent implements OnInit {

  constructor(private gifsService:GifsServiceService ) { }
  // ViewChild permite recoger un elemento del HTML (variable)
  //en este caso "txtFind" para recogerlo y emplearlo en el metodo
  //find()
@ViewChild("txtFind") txtFind!:ElementRef<HTMLInputElement>;
  ngOnInit(): void {
  }

  find=()=>{

    const value=this.txtFind.nativeElement.value;
    console.log(value);
    // if(value.trim().length==0){
    //   return;
    // }

    //Guardamos en el historial de la BBDD
    this.gifsService.findGifs(value);
    this.txtFind.nativeElement.value="";

  }
}
