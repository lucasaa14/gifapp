import { Gif } from './../models/GifResponse';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SearchGIFResponse } from '../models/GifResponse';

@Injectable({
  providedIn: 'root'
})
export class GifsServiceService {
  private _url="https://api.giphy.com/v1/gifs"
  private _apiKey:string="NRrL5nOB9afdJ9TpLTlwm7NRddDAVvRt"
  private _historial:string[]=[];
  public results:Gif[]=[]



   //Retorna historial
   get historial(){
    return [...this._historial]
  }

  constructor(private http:HttpClient) {

      //Recuperar datos guardados en el localStorage
      //JSON.parse ; de esta forma al recargar la pagina mantenemos el historial
if(localStorage.getItem('historial')){
  this._historial = JSON.parse(localStorage.getItem("historial")!)
}
this.results = JSON.parse(localStorage.getItem("results")!) || []
   }


  getGifs=(query:string)=>{
    const params= new HttpParams()
   .set('api_key',this._apiKey)
   .set('limit','10')
   .set('q',query)

    this.http.get<SearchGIFResponse>(`${this._url}/search`,{params:params})//Usamos los headers de la misma forma que usabamos HEADERS
    .subscribe((resp:any)=>{
console.log(resp.data);
console.log(resp);
this.results=resp.data;
localStorage.setItem("results",JSON.stringify(this.results));


    })
  }

//Find gift
  findGifs=(query:string)=>{
query=query.trim().toLowerCase()
    if(query.trim().length==0){
      return;
    }
    //Comprobamos si existe en el array y si no no lo añadimos
    //al historial (includes,unshift)
    //Cortamos el array a 10 posiciones (splice)
    if(!this._historial.includes(query)){
      this._historial.unshift(query);
    }
this._historial=this._historial.splice(0,10);//Cortamos 10 posiciones, el 10 no se incluye
//Guardar en el localStorage las ultimas busquedas transformadas a string
localStorage.setItem("historial",JSON.stringify(this._historial));
this.getGifs(query);
console.log(this._historial);

  }
}
