import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GifsComponent } from './gifs.component';
import { GifsPageComponent } from './gifs-page/gifs-page.component';
import { FindComponent } from './find/find.component';
import { ResultsComponent } from './results/results.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports:[GifsPageComponent],
  declarations: [GifsComponent,
    GifsPageComponent,
    FindComponent,
     ResultsComponent]
})
export class GifsModule { }
